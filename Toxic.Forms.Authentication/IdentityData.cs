﻿using System;
using System.Collections.Generic;
using System.Text;
using Toxic.Forms.Authentication.Models;

namespace Toxic.Forms.Authentication
{
    internal static class IdentityData
    {

        private static RdbContext _db = null;

        internal static Identity GetIdentity(Guid id)
        {
            var result = Db.ExecuteRecord<Identity>("SELECT TOP 1 * FROM Identities WHERE Id = {0}", id);

            return result != null && !result.DeletedWhen.HasValue ? result : null;
        }

        internal static Identity GetIdentity(string emailAddress, bool evenDeleted = false)
        {
            emailAddress = emailAddress.Clean();

            if (emailAddress == null)
            {
                return null;
            }

            var result = Db.ExecuteRecord<Identity>("SELECT TOP 1 * FROM Identities WHERE EmailAddress = {0}", emailAddress);

            return result != null && (evenDeleted || !result.DeletedWhen.HasValue) ? result : null;
        }

        internal static bool SaveIdentity(Identity identity)
        {
            return Db.UpsertRecord("Identities", identity);
        }

        internal static IdentityPasswordResetToken GetPasswordResetToken(string emailAddress)
        {
            return Db.ExecuteRecord<IdentityPasswordResetToken>("SELECT TOP 1 * FROM IdentityPasswordResetTokens WHERE EmailAddress = {0}", emailAddress);
        }

        internal static IdentityPasswordResetToken GetPasswordResetToken(Guid token)
        {
            return Db.ExecuteRecord<IdentityPasswordResetToken>("SELECT TOP 1 * FROM IdentityPasswordResetTokens WHERE token = {0}", token);
        }

        internal static bool SavePasswordResetToken(IdentityPasswordResetToken resetToken)
        {
            return Db.UpsertRecord("IdentityPasswordResetTokens", resetToken);
        }

        private static RdbContext Db
        {
            get
            {
                if (_db == null)
                {
                    _db = ShopCache.Current.Db;
                }
                return _db;
            }
        }
    }
}


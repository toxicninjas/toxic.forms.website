﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Toxic.Forms.Authentication
{
    internal class Salt
    {
        internal string SaltString { get; set; }
        internal byte[] SaltBytes { get; set; }
    }
}

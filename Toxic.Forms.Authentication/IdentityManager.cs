﻿using System;
using System.Collections.Generic;
using System.Text;
using Toxic.Forms.Authentication.Models;

namespace Toxic.Forms.Authentication
{
    public class IdentityManager
    {
		// returnerar Identity (null om den redan fanns)
		public Identity CreateIdentity(string email, string password)
		{
			return IdentityHelper.CreateIdentity(email, password);
		}

		public Identity Login(string email, string password)
		{
			return IdentityHelper.Login(email, password);
		}

		public bool DeleteIdentity(Guid id)
		{
			return IdentityHelper.DeleteIdentity(id);
		}

		public string CreateResetToken(string email, DateTime? expiresWhen = null)
		{
			return IdentityHelper.CreateResetToken(email, expiresWhen);
		}

		public Identity CheckResetToken(Guid token)
		{
			return IdentityHelper.CheckResetToken(token);
		}

		public Identity SetPassword(Guid id, string password)
		{
			return IdentityHelper.SetPassword(id, password);
		}
	}
}

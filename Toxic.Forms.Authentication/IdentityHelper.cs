﻿using System;
using System.Security.Cryptography;

namespace Toxic.Forms.Authentication
{
    internal class IdentityHelper
    {
        private static Salt GenerateSalt(int size = 16)
        {
            var cryptoProvider = new RNGCryptoServiceProvider();

            var saltBytes = new Byte[size];
            cryptoProvider.GetNonZeroBytes(saltBytes);

            return new Salt { SaltString = Convert.ToBase64String(saltBytes), SaltBytes = saltBytes };
        }

        private static string HashPassword(string password, Salt salt)
        {
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt.SaltBytes, 10000);
            var hashPassword = Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256));

            return hashPassword;
        }

        private static bool VerifyPassword(string enteredPassword, string storedHash, string storedSalt)
        {
            enteredPassword = enteredPassword.Clean();
            storedHash = storedHash.Clean();
            storedSalt = storedSalt.Clean();

            if (enteredPassword == null || storedHash == null || storedSalt == null)
            {
                return false;
            }

            var saltBytes = Convert.FromBase64String(storedSalt);
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(enteredPassword, saltBytes, 10000);

            return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256)) == storedHash;
        }

        // Note: We overwrite existing if it is marked as deleted, but throw an exception
        // if it is not.
        internal static Identity CreateIdentity(string emailAddress, string password = null)
        {
            emailAddress = emailAddress.Clean();
            password = password.Clean();

            if (emailAddress == null)
            {
                throw new ArgumentException("E-mail address cannot be null", nameof(emailAddress));
            }

            var identity = IdentityData.GetIdentity(emailAddress);

            if (identity != null && identity.DeletedWhen != null)
            {
                throw new Exception($"The identity ({emailAddress}) already existed");
            }

            var salt = password != null ? GenerateSalt() : null;
            var hash = password != null ? HashPassword(password, salt) : null;

            identity = new Identity
            {
                Id = Guid.NewGuid(),
                Salt = salt?.SaltString,
                EmailAddress = emailAddress,
                Hash = hash,
                CreatedWhen = DateTime.Now
            };

            if (!IdentityData.SaveIdentity(identity))
            {
                throw new Exception($"Could not create Identity. Something went wrong");
            }

            return identity;
        }

        internal static Identity Login(string emailAddress, string password)
        {
            var identity = IdentityData.GetIdentity(emailAddress);

            if (identity != null && !VerifyPassword(password, identity.Hash, identity.Salt))
            {
                identity = null;
            }

            return identity;
        }

        internal static string CreateResetToken(string emailAddress, DateTime? expiresWhen)
        {
            Guid token = Guid.Empty;

            // TODO: This is wrong:
            if (IdentityData.GetPasswordResetToken(emailAddress) == null)
            {
                var identity = IdentityData.GetIdentity(emailAddress);

                if (identity != null)
                {
                    token = GenerateResetToken();

                    var resetToken = new IdentityPasswordResetToken
                    {
                        Id = Guid.NewGuid(),
                        IdentityId = identity.Id,
                        EmailAddress = identity.EmailAddress,
                        Token = token,
                        ExpiresWhen = expiresWhen.HasValue ? expiresWhen.Value : DateTime.Now.AddHours(24)
                    };

                    if (!IdentityData.SavePasswordResetToken(resetToken))
                    {
                        throw new Exception($"Could not create Identity password reset token. Something went wrong");
                    }
                }
            }

            return token != Guid.Empty ? token.ToString() : null;
        }

        internal static Identity CheckResetToken(Guid token)
        {
            if (token != Guid.Empty)
            {
                var resetToken = IdentityData.GetPasswordResetToken(token);

                if (resetToken != null && !resetToken.Expired && !resetToken.TokenUsed)
                {
                    // TODO: Maybe this is too soon?
                    resetToken.TokenUsed = true;

                    if (IdentityData.SavePasswordResetToken(resetToken))
                    {
                        return IdentityData.GetIdentity(resetToken.IdentityId);
                    }
                }
            }

            return null;
        }

        internal static Identity SetPassword(Guid id, string password)
        {
            var identity = IdentityData.GetIdentity(id);

            if (identity != null)
            {
                password = password.Clean();

                var salt = password != null ? GenerateSalt() : null;
                var hash = password != null ? HashPassword(password, salt) : null;

                identity.Salt = salt?.SaltString;
                identity.Hash = hash;

                IdentityData.SaveIdentity(identity);

                return identity;
            }
            else
            {
                throw new Exception($"Could not find and update password for Identity with id {id}");
            }
        }

        internal static bool DeleteIdentity(Guid id)
        {
            bool deleted = false;
            var identity = IdentityData.GetIdentity(id);

            if (identity != null)
            {
                identity.DeletedWhen = DateTime.Now;
                deleted = IdentityData.SaveIdentity(identity);
            }

            return deleted;
        }

        private static Guid GenerateResetToken()
        {
            return Guid.NewGuid();
        }
    }
}
}

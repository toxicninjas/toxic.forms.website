﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Toxic.Forms.Authentication.Models
{
    public class Identity
    {
        public Guid Id { get; set; }
        public string EmailAddress { get; set; }
        public string Salt { get; set; }
        public string Hash { get; set; }
        public DateTime CreatedWhen { get; set; }
        public DateTime? DeletedWhen { get; set; } = null;
    }
}

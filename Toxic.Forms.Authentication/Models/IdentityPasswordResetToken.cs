﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Toxic.Forms.Authentication.Models
{
    internal class IdentityPasswordResetToken
    {
        internal Guid Id { get; set; }
        internal Guid IdentityId { get; set; }
        internal string EmailAddress { get; set; }
        internal Guid Token { get; set; }
        internal DateTime ExpiresWhen { get; set; }
        internal bool TokenUsed { get; set; }

        internal bool Expired
        {
            get
            {
                return DateTime.Compare(ExpiresWhen, DateTime.Now) < 0 ? true : false;
            }
        }
    }
}
